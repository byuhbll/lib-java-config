package edu.byu.hbll.config;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;

import org.yaml.snakeyaml.Yaml;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Loads YAML data into JsonNodes and/or target POJOs.
 * 
 * <p>
 * This loader uses Jackson's YAML plugin to do the actual parsing of data. As such, this class follows Jackson's own
 * recommendations regarding thread-safeness; that is that instances of this class are fully thread-safe provided that
 * ALL configuration of the internal ObjectMapper occurs before any loading/deserialization takes place.
 * 
 * <p>
 * YAML documents containing an \@import field as an array of file paths will recursively import the YAML documents
 * found at those paths giving precedence to the YAML document doing the import.
 */
public class YamlLoader {
    
    private ObjectMapper objectMapper = new ObjectMapper();
    
    /**
     * We use SnakeYAML directly vs `new ObjectMapper(new YAMLFactory())` because the latter does not support all
     * advanced YAML features and syntax.
     */
    private Yaml yaml = new Yaml();
    
    /**
     * Deserializes the YAML data contained in the provided readers into a single JsonNode. If more than one reader is
     * provided, then the first reader will be used to established a baseline node; subsequent readers will merge into
     * that baseline node in sequence, adding or overwriting fields as appropriate in a cascading fashion.
     * 
     * @param readers the readers containing the raw YAML data
     * @return the merged JsonNode
     * @throws JsonProcessingException if a failure occurs while parsing the JSON data
     * @throws IOException if one or more of the readers fails
     */
    public JsonNode load(Reader... readers) throws JsonProcessingException, IOException {
        return load(null, Paths.get(""), readers);
    }
    
    /**
     * Deserializes the YAML data contained in the provided readers into the provided Jackson POJO. If more than one
     * reader is provided, then the first reader will be used to established a baseline JsonNode; subsequent readers
     * will merge into that baseline node in sequence, adding or overwriting fields as appropriate in a cascading
     * fashion before the final boxing of the data into the target POJO type.
     * 
     * @param readers the readers containing the raw YAML data
     * @return the target object containing the parsed data
     * @throws JsonProcessingException if a failure occurs while parsing the JSON data or while boxing the result
     * @throws IOException if one or more of the readers fails
     */
    public <T> T load(Class<T> targetType, Reader... readers) throws JsonProcessingException, IOException {
        return objectMapper.treeToValue(load(readers), targetType);
    }
    
    /**
     * Deserializes the YAML data contained in the provided files into a single JsonNode. If more than one file is
     * provided, then the first file will be used to established a baseline node; subsequent files will merge into that
     * baseline node in sequence, adding or overwriting fields as appropriate in a cascading fashion.
     * 
     * @param paths the files containing the raw YAML data
     * @return the merged JsonNode
     * @throws JsonProcessingException if a failure occurs while parsing the JSON data
     * @throws IOException if a failure occurs while reading data from one or more of the files
     */
    public JsonNode load(Path... paths) throws JsonProcessingException, IOException {
        JsonNode mainNode = null;
        
        for (Path path : paths) {
            try (Reader reader = Files.newBufferedReader(path)) {
                mainNode = load(mainNode, path.getParent(), reader);
            }
        }
        
        return mainNode;
    }
    
    /**
     * Deserializes the YAML data contained in the provided files into the provided Jackson POJO. If more than one file
     * is provided, then the first file will be used to established a baseline JsonNode; subsequent files will merge
     * into that baseline node in sequence, adding or overwriting fields as appropriate in a cascading fashion before
     * the final boxing of the data into the target POJO type.
     * 
     * @param paths the files containing the raw YAML data
     * @return the target object containing the parsed data
     * @throws JsonProcessingException if a failure occurs while parsing the JSON data or while boxing the result
     * @throws IOException if a failure occurs while reading data from one or more of the files
     */
    public <T> T load(Class<T> targetType, Path... paths) throws JsonProcessingException, IOException {
        return objectMapper.treeToValue(load(paths), targetType);
    }
    
    /**
     * Deserializes the YAML data contained in the provided readers into a single JsonNode. If more than one reader is
     * provided, then the first reader will be used to established a baseline node; subsequent readers will merge into
     * that baseline node in sequence, adding or overwriting fields as appropriate in a cascading fashion. Any YAML
     * documents containing an \@import field as an array of file paths will recursively import the YAML documents found
     * at those paths giving precedence to the YAML document doing the import.
     * 
     * @param mainNode the base json node to receive updates, use null if starting empty
     * @param directory the directory used for constructing relative paths
     * @param readers the readers containing the raw YAML data
     * @return the merged JsonNode
     * @throws JsonProcessingException if a failure occurs while parsing the JSON data
     * @throws IOException if one or more of the readers fails
     */
    private JsonNode load(JsonNode mainNode, Path directory, Reader... readers)
            throws JsonProcessingException, IOException
    {
        for (Reader reader : readers) {
            JsonNode thisNode = objectMapper.valueToTree(yaml.load(reader));
            
            for (JsonNode importField : thisNode.path("@import")) {
                JsonNode updateNode = load(directory.resolve(importField.asText()));
                mainNode = merge(mainNode, updateNode);
            }
            
            mainNode = merge(mainNode, thisNode);
        }
        
        return mainNode;
    }
    
    /**
     * Merges data from the second argument into the JsonNode provided as the first argument.
     * 
     * Derived from a method contributed to http://stackoverflow.com/a/11459962 by StackOverflow users arne
     * (http://stackoverflow.com/users/838776/arne) and c4k (http://stackoverflow.com/users/1259118/c4k).
     * 
     * @param mainNode the node containing the baseline, original data
     * @param updateNode the node containing new data to merge
     */
    JsonNode merge(JsonNode mainNode, JsonNode updateNode) {
        
        // Update node is an array
        if (mainNode == null || !mainNode.isObject() || !updateNode.isObject()) {
            return updateNode;
        }
        
        // Update node is an object
        Iterator<String> fieldNames = updateNode.fieldNames();
        while (fieldNames.hasNext()) {
            String fieldName = fieldNames.next();
            JsonNode currentMainNode = mainNode.path(fieldName);
            JsonNode currentUpdateNode = updateNode.path(fieldName);
            if (currentMainNode.isObject() && currentUpdateNode.isObject()) {
                // Recursive case
                merge(currentMainNode, currentUpdateNode);
            } else if (mainNode.isObject()) {
                // Base Case
                ((ObjectNode) mainNode).replace(fieldName, currentUpdateNode);
            }
        }
        return mainNode;
    }
    
    /**
     * Returns the ObjectMapper used to load/deserialize YAML files. Clients may modify this ObjectMapper to meet their
     * specific needs.
     * 
     * @return the objectMapper
     */
    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }
    
}
