package edu.byu.hbll.config;

import static org.junit.Assert.*;

import java.io.File;
import java.io.Reader;
import java.io.StringReader;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Unit tests (and a couple integration tests that read from a yaml file included with this project) for YamlLoader.
 */
public class YamlLoaderTest {
    
    private static String simpleFieldSourceA = "---" + System.lineSeparator()
            + "key : a";
    
    private static String simpleFieldSourceB = "---" + System.lineSeparator()
            + "key : b";
    
    private static String simpleFieldSourceNull = "---" + System.lineSeparator()
            + "key : null";
    
    private static String arraySourceAB = "---" + System.lineSeparator()
            + "- elementA" + System.lineSeparator()
            + "- elementB" + System.lineSeparator();
    
    private static String arraySourceC = "---" + System.lineSeparator()
            + "- elementC";
    
    private static String objectSourceAB = "---" + System.lineSeparator()
            + "key: " + System.lineSeparator()
            + "  a : alpha" + System.lineSeparator()
            + "  b : bravo";
    
    private static String objectSourceAC = "---" + System.lineSeparator()
            + "key : " + System.lineSeparator()
            + "  a : new alpha" + System.lineSeparator()
            + "  c : charlie";
    
    private static String fullSource1 = "---" + System.lineSeparator()
            + "intVar : 1" + System.lineSeparator()
            + "strVar : value" + System.lineSeparator()
            + "objVar : " + System.lineSeparator()
            + "  objChild : childValue";
    
    private static String fullSource2 = "---" + System.lineSeparator()
            + "strVar : updatedStrValue";
    
    private static String fullSource3 = "---" + System.lineSeparator()
            + "newVar : newValue" + System.lineSeparator()
            + "objVar : " + System.lineSeparator()
            + "  newChild : newChildValue";
    
    private static Path sourcePath = Paths.get("src/test/resources/test.yml");
    
    private static YamlLoader loader = new YamlLoader();
    private static ObjectMapper mapper = new ObjectMapper();
    
    /**
     * The merge method should fully overwrite/replace a simple field with a null value. Since the null value is itself
     * a simple field, if this test passes, it is assumed that overwriting an array and a complex object will also work
     * assuming that they can be overwritten by a simple field.
     * 
     * @throws Exception if any exception occurs while running the test
     */
    @Test
    public void merge_shouldOverwriteFieldWithNull() throws Exception {
        JsonNode a = loader.load(new StringReader(simpleFieldSourceA));
        JsonNode b = loader.load(new StringReader(simpleFieldSourceNull));
        
        JsonNode result = loader.merge(a, b);
        assertNull(result.get("key").asText(null));
    }
    
    /**
     * The merge method should fully overwrite/replace a simple field with any other simple field.
     * 
     * @throws Exception if any exception occurs while running the test
     */
    @Test
    public void merge_shouldOverwriteFieldWithOtherField() throws Exception {
        JsonNode a = loader.load(new StringReader(simpleFieldSourceA));
        JsonNode b = loader.load(new StringReader(simpleFieldSourceB));
        
        JsonNode result = loader.merge(a, b);
        assertEquals("b", result.get("key").asText());
    }
    
    /**
     * The merge method should fully overwrite/replace a simple field with an array.
     * 
     * @throws Exception if any exception occurs while running the test
     */
    @Test
    public void merge_shouldOverwriteFieldWithArray() throws Exception {
        JsonNode a = loader.load(new StringReader(simpleFieldSourceA));
        JsonNode b = loader.load(new StringReader(arraySourceAB));
        
        JsonNode result = loader.merge(a, b);
        assertTrue(result.isArray());
        assertEquals("elementA", result.get(0).asText(null));
        assertEquals("elementB", result.get(1).asText(null));
        assertNull(result.get(2));
    }
    
    /**
     * The merge method should fully overwrite/replace a simple field with a complex object.
     * 
     * @throws Exception if any exception occurs while running the test
     */
    @Test
    public void merge_shouldOverwriteFieldWithObject() throws Exception {
        JsonNode a = loader.load(new StringReader(simpleFieldSourceA));
        JsonNode b = loader.load(new StringReader(objectSourceAB));
        
        JsonNode result = loader.merge(a, b);
        assertTrue(result.get("key").isObject());
        assertEquals("alpha", result.get("key").get("a").asText());
        assertEquals("bravo", result.get("key").get("b").asText());
    }
    
    /**
     * The merge method should fully overwrite/replace an array with a simple field.
     * 
     * @throws Exception if any exception occurs while running the test
     */
    @Test
    public void merge_shouldOverwriteArrayWithField() throws Exception {
        JsonNode a = loader.load(new StringReader(arraySourceAB));
        JsonNode b = loader.load(new StringReader(simpleFieldSourceA));
        
        JsonNode result = loader.merge(a, b);
        assertEquals("a", result.get("key").asText());
        assertNull(result.get(1));
    }
    
    /**
     * The merge method should fully overwrite/replace an array with another array.
     * 
     * @throws Exception if any exception occurs while running the test
     */
    @Test
    public void merge_shouldOverwriteArrayWithOtherArray() throws Exception {
        JsonNode a = loader.load(new StringReader(arraySourceAB));
        JsonNode b = loader.load(new StringReader(arraySourceC));
        
        JsonNode result = loader.merge(a, b);
        assertTrue(result.isArray());
        assertEquals("elementC", result.get(0).asText());
        assertNull(result.get(1));
    }
    
    /**
     * The merge method should fully overwrite/replace an array with a complex object.
     * 
     * @throws Exception if any exception occurs while running the test
     */
    @Test
    public void merge_shouldOverwriteArrayWithObject() throws Exception {
        JsonNode a = loader.load(new StringReader(arraySourceAB));
        JsonNode b = loader.load(new StringReader(objectSourceAB));
        
        JsonNode result = loader.merge(a, b);
        assertTrue(result.get("key").isObject());
        assertEquals("alpha", result.get("key").get("a").asText());
        assertEquals("bravo", result.get("key").get("b").asText());
    }
    
    /**
     * The merge method should fully overwrite/replace a complex object with a simple field.
     * 
     * @throws Exception if any exception occurs while running the test
     */
    @Test
    public void merge_shouldOverwriteObjectWithField() throws Exception {
        JsonNode a = loader.load(new StringReader(objectSourceAB));
        JsonNode b = loader.load(new StringReader(simpleFieldSourceA));
        
        JsonNode result = loader.merge(a, b);
        assertFalse(result.get("key").isObject());
        assertEquals("a", result.get("key").asText());
    }
    
    /**
     * The merge method should fully overwrite/replace a complex object with an array.
     * 
     * @throws Exception if any exception occurs while running the test
     */
    @Test
    public void merge_shouldOverwriteObjectWithArray() throws Exception {
        JsonNode a = loader.load(new StringReader(objectSourceAB));
        JsonNode b = loader.load(new StringReader(arraySourceC));
        
        JsonNode result = loader.merge(a, b);
        assertTrue(result.isArray());
        assertEquals("elementC", result.get(0).asText());
        assertNull(result.get(1));
    }
    
    /**
     * The merge method should recursively merge two complex objects.
     * 
     * @throws Exception if any exception occurs while running the test
     */
    @Test
    public void merge_shouldMergeTwoObjects() throws Exception {
        JsonNode a = loader.load(new StringReader(objectSourceAB));
        JsonNode b = loader.load(new StringReader(objectSourceAC));
        
        assertEquals(a, loader.merge(a, b));
        assertEquals("new alpha", a.path("key").path("a").asText());
        assertEquals("bravo", a.path("key").path("b").asText());
        assertEquals("charlie", a.path("key").path("c").asText());
    }
    
    /**
     * The load(Reader...readers) method should fail with a NullPointerException if only null values are provided.
     * 
     * @throws Exception if any exception occurs while running the test
     */
    @Test(expected = NullPointerException.class)
    public void load_shouldFailIfNoReadersProvided() throws Exception {
        loader.load((Reader) null);
    }
    
    /**
     * The load(Reader...readers) method should fail with a NullPointerException if any null values are provided.
     * 
     * @throws Exception if any exception occurs while running the test
     */
    @Test(expected = NullPointerException.class)
    public void load_shouldFailIfAnyNullReadersProvided() throws Exception {
        loader.load(
                new StringReader(fullSource1),
                (Reader) null,
                new StringReader(fullSource2));
    }
    
    /**
     * The load(Reader...readers) method should return the appropriate JsonNode for a single source.
     * 
     * @throws Exception if any exception occurs while running the test
     */
    @Test
    public void load_shouldProperlyBuildJsonNodeFromSingleReader() throws Exception {
        JsonNode node = loader.load(new StringReader(fullSource1));
        
        assertEquals(1, node.path("intVar").asInt());
        assertEquals("value", node.path("strVar").asText());
        assertEquals("childValue", node.path("objVar").path("objChild").asText());
    }
    
    /**
     * The load(Reader...readers) method should return the appropriate JsonNode for a multiple sources. The specific
     * logic for merging the data from multiple nodes is tested in the merge_* test methods.
     * 
     * @throws Exception if any exception occurs while running the test
     */
    @Test
    public void load_shouldBuildJsonNodeFromMultipleReader() throws Exception {
        JsonNode node = loader.load(
                new StringReader(fullSource1),
                new StringReader(fullSource2),
                new StringReader(fullSource3));
        
        assertEquals(1, node.path("intVar").asInt());
        assertEquals("updatedStrValue", node.path("strVar").asText());
        assertEquals("childValue", node.path("objVar").path("objChild").asText());
        assertEquals("newChildValue", node.path("objVar").path("newChild").asText());
        assertEquals("newValue", node.path("newVar").asText());
    }
    
    /**
     * The load(Class targetType, Reader...readers) should return a populated target POJO rather than a raw JsonNode.
     * <p>
     * Only the target/POJO piece of this method is tested here, since the previous tests have complete coverage on the
     * underlying logic.
     * 
     * @throws Exception if any exception occurs while running the test
     */
    @Test
    public void load_shouldProperlyBuildTargetClassFromReaders() throws Exception {
        ExampleTarget target = loader.load(ExampleTarget.class, new StringReader(fullSource1));
        
        assertEquals(Integer.valueOf(1), target.intVar);
        assertEquals("value", target.strVar);
        assertEquals("childValue", target.objVar.objChild);
    }
    
    /**
     * The load(Path...paths) should return the appropriate JsonNode for one or more paths.
     * <p>
     * Only the use of Path instead of Reader is tested here, since the previous tests have complete coverage on the
     * underlying logic.
     * 
     * @throws Exception if any exception occurs while running the test
     */
    @Test
    public void load_shouldProperlyBuildJsonNodeFromPaths() throws Exception {
        JsonNode node = loader.load(sourcePath);
        
        assertEquals(1, node.path("intVar").asInt());
        assertEquals("value", node.path("strVar").asText());
        assertEquals("childValue", node.path("objVar").path("objChild").asText());
    }
    
    /**
     * The load(Class targetType, Path...paths) should return a populated target POJO for one or more paths.
     * <p>
     * Only the use of Path instead of Reader is tested here, since the previous tests have complete coverage on the
     * underlying logic.
     * 
     * @throws Exception if any exception occurs while running the test
     */
    @Test
    public void load_shouldProperlyBuildTargetClassFromPaths() throws Exception {
        ExampleTarget target = loader.load(ExampleTarget.class, sourcePath);
        
        assertEquals(Integer.valueOf(1), target.intVar);
        assertEquals("value", target.strVar);
        assertEquals("childValue", target.objVar.objChild);
    }
    
    /**
     * Verifies that yaml files and their imports are loaded with correct precedence levels. The test yaml files are set
     * up to be loaded in alphabetical order (lowest to highest precedence).
     * 
     * @throws Exception if any exception occurs while running the test
     */
    @Test
    public void testImports() throws Exception {
        JsonNode node = loader.load(Paths.get("src/test/resources/import/c.yml"),
                Paths.get("src/test/resources/import/f.yml"));
        
        for (char c = 'a'; c <= 'f'; c++) {
            // each value should equal its key name
            assertEquals(c + "", node.path(c + "").asText());
        }
    }
    
    /**
     * Tests advanced YAML features and syntax.
     * 
     * @throws Exception
     */
    @Test
    public void testAdvanced() throws Exception {
        JsonNode expected = mapper.readTree(new File("src/test/resources/advanced.json"));
        JsonNode actual = loader.load(Paths.get("src/test/resources/advanced.yml"));
        assertEquals(expected, actual);
    }
    
    /**
     * Example target bean for testing the ability of YamlLoader to parse YAML into a POJO.
     */
    public static class ExampleTarget {
        
        @JsonProperty
        Integer intVar;
        @JsonProperty
        String strVar;
        @JsonProperty
        ObjVar objVar;
        
        public static class ObjVar {
            
            @JsonProperty
            String objChild;
            
        }
        
    }
    
}
