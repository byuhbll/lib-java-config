# Configuration Library for Java Projects

## Deprecation Notice

This project is deprecated. It is no longer supported and should not be used.  Users should use https://gitlab.com/byuhbll/lib/java/json instead.

## Introduction

[![Build Status](https://semaphoreci.com/api/v1/byuhbll/lib-java-config/branches/master/shields_badge.svg)](https://semaphoreci.com/byuhbll/lib-java-config)

The config library provides support for loading YAML configurations into an application.  Parsing is done using the
YAML dataformat plugin for Jackson, which provides a great deal of flexibility for working with the output.  Support is
provided for returning a generic JsonNode as well as a populated POJO using Jackson mapping.

Additionally, the cascaded loading of multiple configuration sources is supported.  When multiple data sources are
provided, values added or updated in sources listed later will append or replace the data parsed from sources listed
earlier.  Fields defined in earlier data sources but not referenced by later data sources will be preserved.

An example of cascaded loading is demonstrated below.  In this example, loading `source1.yml` and `source2.yml` in
a cascading fashion results in an effective YAML configuration identical to `source3.yml`.  Note that the additional
fields in `source2.yml` were added, while the shared key was updated and unreferenced keys were left untouched.

```java

YamlLoader loader = new CascadingYamlLoader();
JsonNode a = loader.load(Paths.get("source1.yml"), Paths.get("source2.yml"));
JsonNode b = loader.load(Paths.get("source3.yml"));

a.equals(b);  //Returns TRUE
```

###source1.yml
```yaml

---
protocol : https
host : localhost
port : 443
```

###source2.yml
```yaml

---
host : localhost
username : myUser
password : myPassword
```

###source3.yml
```yaml

---
protocol : https
host : localhost
port : 443
username : myUser
password : myPassword
```

## Imports
The configuration library also supports file imports from within a YAML document even though YAML does not natively support imports. To import another YAML file, add a field named `@import` at the root of the document and specify the files to import.

```yaml
"@import": "path/to/file"
```

or specify multiple files.

```yaml
"@import": ["path/to/file1", "path/to/file2"]
```

The import paths are relative to the path from which the current YAML was loaded or the current working directory if the YAML document was not loaded from a file. Absolute paths can be used as well. Imports take a lower precedence than the current YAML document and imports later in the list have a higher precedence than those appearing earlier in the list.
